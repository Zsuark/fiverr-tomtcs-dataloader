(defproject fiverr-tomtcs-dataloader "1.2.0"
  :description "Data loader tool for tomtcs on Fiverr"
  :url "https://bitbucket.org/Zsuark/fiverr-tomtcs-dataloader"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/core.async "0.3.443"]
                 [org.clojure/java.jdbc "0.7.1"]
                 [mysql/mysql-connector-java "5.1.44"]
                 [clojure.java-time "0.3.0"]]
  :main ^:skip-aot fiverr-tomtcs-dataloader.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})

; git@bitbucket.org:Zsuark/fiverr-tomtcs-dataloader.git
