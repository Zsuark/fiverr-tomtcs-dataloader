# fiverr-tomtcs-dataloader

- Data loader tool created for tomtcs on Fiverr.
- Order #FO6E056A64A4
- Order placed: 13th September, 2017.


Within the doc folder of the source code, you will find:

- `doc/MySQL_help.docx` for the original problem description
- `doc/MySQL_help.pdf` for the original problem description, and my notes on the analysis of the problem.


## License

Copyright © 2017 Raphael Krausz

Distributed under the Eclipse Public License version 1.0.

The above Eclipse license is available in the file LICENSE included with the source code. The original license is hosted at [http://www.eclipse.org/legal/epl-v10.html](http://www.eclipse.org/legal/epl-v10.html).

Contact Raphael via:

- [https://www.fiverr.com/zsuark](https://www.fiverr.com/zsuark)
- [https://bitbucket.org/Zsuark/](https://bitbucket.org/Zsuark/)
- [https://github.com/Zsuark](https://github.com/Zsuark)
- [https://www.linkedin.com/in/raphael-krausz-7704233b/](https://www.linkedin.com/in/raphael-krausz-7704233b/)


## Overview

The software searches for tables fitting a matching pattern on one MySQL server, it extracts the data from these tables and places it into a common table on another MySQL server.

We will call the first server, the source - as that is where the data is coming from, and likewise the second the target.

Transformation:

- The source table the row is extracted from is stored as `original_table` in the target database.
- The original id from the extracted row is stored in the target field `original_id`.
- All other fields are preserved.
- The primary key of the target table is a composite key, consisting of `original_table` and `original_id`.


Modes of operation:

- The software has two modes:
	- an incremental mode - only today's and yesterday's new data is added.
	- a full mode - all new data from the source database is transferred
- The mode may be chosen from the `:only-incrementals` option in the config file.

No data is duplicated in the target database. You may run the software as many times as you like, but only new information will be added to the target database.

Be aware though, that the incremental mode will run much faster.

## Quickstart

First edit `conf/config.edn` with your favourite text editor.

Execute the application:

    $ java -jar tomtcs-dataloader.jar


## Configuring the software

After you've extracted the software from it's zip archive, you will find the following files and directories:

- `tomtcs-dataloader.jar` - a standalone JAR file, which you may execute with java
- `conf` - a configuration directory.
- `conf/config.edn` - and EDN format config file, please edit this with your favourite text editor.
- `conf/target_table.sql` - no need to edit this, except if you need to alter the target table's definition.
   - `::TargetTableName::` will be replaced by the actual table's name in the softare, so please make sure this is preserved if you edit.


### Configuration settings

- `:source-db` - the database specifications of "Server A" in your description
- `:target-db` - the database specifications of "Server B" in your description
	- the parameters should be straightforward, let me know if you need further assistance
- `:target-table` - the name of table in the target database. The table the data is going to.
- `:source-pattern` - the MySQL LIKE pattern for identifying tables in the source database. The tables where the data is coming from.
- `:source-incremental-pattern` - A combination of the MySQL LIKE pattern and a replaceable date format between an opening `::` and closing `::`. This pattern is used to identify source tables containing a specific date.
- `:only-incrementals` - set to `true` to process only data coming on the day the software is run, and the previous day.

**Please note:**

- In the configuration file, as Java strings use a blackslash (\\) as an escape character, as does MySQL - so two backslashes need to be used to indicate one backslash in MySQL.
- Boolean values must be given as `true` or `false` without any quotes.
	- this applies to `:only-incrementals`, and the `:useSSL` option in the database specification configurations.


## Technical information

### IMPORTANT Security & Warnings

- The config file `conf/config.edn` contains sensitive information. Make sure the file is kept secure and is not readable by any other users!
- The source-db user must ONLY have READ permissions on the source database.
- The target-db user must have WRITE permissions to the target database.
  - If the target table needs to be created on the target database, the target-db user also requires permission to CREATE the table.
- Don't use the MySQL root user - create a specific user for each role.

### Database

- The SQL specifications described in MySQL_help.docx was used as a basis for the target table.
- All randomly generated test data was generated according to this information.
- The software attempts to create the target table if it does not exists.
- If the target table is missing, and cannot be created due to SQL modes, you will see an error message which includes the text: `Invalid default value for 'DID2004'`


## Usage

Please first configure the correct parameters in config.edn

	    $ java -jar tomtcs-dataloader.jar

**Ensure the `config.edn` file is correct and the `conf` folder is in the current working directory where you execute the java command.**


### Software Language used

The software is written in Clojure 1.8.0. It may be compiled and run on Java versions 6, 7 or 8.

The delivered binaries were built on top of Java 8.

If you need to recompile, use the [Leiningen](https://leiningen.org/) build tool.

From the source distribution root level directory, run:

		$ lein uberjar

You will find the jar file in `target/uberjar/fiverr-tomtcs-dataloader-1.2.0-standalone.jar`.
