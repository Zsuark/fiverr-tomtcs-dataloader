; WARNING!
; DO NOT USE THESE FUNCTIONS UNLESS YOU UNDERSTAND WHAT YOU ARE DOING
; They will write dummy data to a system.
; DO NOT USE ON A PRODUCTION SERVER!!!!!!!!!!!!!

; If you still wish to proceed generateTestData is the function
; you are after

(ns fiverr-tomtcs-dataloader.generate-test-data
  (:gen-class)
  (:require [clojure.java.jdbc :as j])
  (:require [clojure.core.async :as async]))



(defn random-datetime []
  (let [
        year	(str "20" (format "%02d", (rand-int 18)))
        month   (format "%02d", (inc (rand-int 12)))
        day     (format "%02d",
                        (inc (if (= month "02") (rand-int 28) (rand-int 30))))
        hour    (format "%02d", (rand-int 24))
        minute  (format "%02d", (rand-int 60))
        sec     (format "%02d", (rand-int 60))
        ]
    (str year "-" month "-" day " " hour ":" minute ":" sec)))


(defn fields
  "Returns a randomly populated test data row"
  []
  {
   ; requiredFields {
   :sart_instance_id	(rand-int 999999999)
   :sal_file_id			(rand-int 9999)
   :call_id				(rand-int 999999999)
   :duration			(rand-int 999999999)
   :Start_Time			(random-datetime)
   :DID2004				(random-datetime)
   :calltype			(rand-int 10)
   :callstate			(rand-int 10)
   ;                 }
   ; optionalFields {
   :DID1017  (rand-int 999999999)
   :DID1019 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID1020 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID1021 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID1023 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID1024 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID1025 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID2005  (rand-int 65536)
   :DID2020 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID2024 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID2023 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID2025 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID4000 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID4001 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID4003  (rand-int 65536)
   :DID4004  (rand-int 65536)
   :DID4005  (rand-int 65536)
   :DID3000  (rand-int 65536)
   :DID3001  (rand-int 65536)
   :DID3002  (rand-int 65536)
   :DID5500 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID5501 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID5502  (rand-int 65536)
   :DID5503 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID5504 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID2500 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID6000 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID6001 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID6010 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID6011 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID6012  (rand-int 65536)
   :DID6013 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID6014 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID6015 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID6016 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID6017 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID6050 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID6051 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID6052  (rand-int 65536)
   :DID6053  (rand-int 65536)
   :DID6054 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID1026  (rand-int 999999999)
   :DID1027  (rand-int 999999999)
   :DID1028  (rand-int 999999999)
   :DID1029  (rand-int 65536)
   :DID1030  (rand-int 65536)
   :DID1040 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID1031  (rand-int 65536)
   :DID1032  (rand-int 65536)
   :DID2009  (rand-int 65536)
   :DID2006 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID2007 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID2008 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID1033  (rand-int 999999999)
   :DID1034  (rand-int 999999999)
   :DID1035  (rand-int 999999999)
   :DID1036  (rand-int 65536)
   :DID1037  (rand-int 65536)
   :DID1041 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID1038  (rand-int 65536)
   :DID1039  (rand-int 65536)
   :DID2013  (rand-int 65536)
   :DID2010 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID2011 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID2012 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID2030 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID2031  (rand-int 999999999)
   :DID2032  (rand-int 999999999)
   :DID2033  (rand-int 999999999)
   :DID2034  (rand-int 999999999)
   :DID2035  (rand-int 999999999)
   :DID2036  (rand-int 999999999)
   :DID2037  (rand-int 999999999)
   :DID2060  (rand-int 999999999)
   :DID2061  (rand-int 999999999)
   :DID2062  (rand-int 999999999)
   :DID2046 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID2047 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID2048 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID2049 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID2050 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID2051 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID2052 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID2053 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID2054 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID2055 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID2056 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID2057 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID2058 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID4100 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID4101 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID4102 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID4103 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID4104  (rand-int 65536)
   :DID4105 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID4106 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID4107  (rand-int 65536)
   :DID4109 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   :DID4108  (rand-int 65536)
   :DID4111 "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
   })


(def tableTemplate
  "CREATE TABLE IF NOT EXISTS `::TableName::` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `sart_instance_id` int(10) unsigned NOT NULL,
  `sal_file_id` smallint(5) unsigned NOT NULL,
  `call_id` int(10) unsigned NOT NULL,
  `duration` bigint(20) unsigned NOT NULL,
  `Start_Time` timestamp NOT NULL,
  `DID1017` bigint(20) DEFAULT NULL,
  `DID2004` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `DID1019` varchar(255) DEFAULT NULL,
  `DID1020` varchar(255) DEFAULT NULL,
  `DID1021` varchar(255) DEFAULT NULL,
  `DID1023` varchar(255) DEFAULT NULL,
  `DID1024` varchar(255) DEFAULT NULL,
  `DID1025` varchar(255) DEFAULT NULL,
  `DID2005` int(10) unsigned DEFAULT NULL,
  `DID2020` varchar(255) DEFAULT NULL,
  `DID2024` varchar(255) DEFAULT NULL,
  `DID2023` varchar(255) DEFAULT NULL,
  `DID2025` varchar(255) DEFAULT NULL,
  `DID4000` varchar(255) DEFAULT NULL,
  `DID4001` varchar(255) DEFAULT NULL,
  `DID4003` int(10) unsigned DEFAULT NULL,
  `DID4004` int(10) unsigned DEFAULT NULL,
  `DID4005` int(10) unsigned DEFAULT NULL,
  `DID3000` int(10) unsigned DEFAULT NULL,
  `DID3001` int(10) unsigned DEFAULT NULL,
  `DID3002` int(10) unsigned DEFAULT NULL,
  `DID5500` varchar(255) DEFAULT NULL,
  `DID5501` varchar(255) DEFAULT NULL,
  `DID5502` int(10) unsigned DEFAULT NULL,
  `DID5503` varchar(255) DEFAULT NULL,
  `DID5504` varchar(255) DEFAULT NULL,
  `DID2500` varchar(255) DEFAULT NULL,
  `DID6000` varchar(255) DEFAULT NULL,
  `DID6001` varchar(255) DEFAULT NULL,
  `DID6010` varchar(255) DEFAULT NULL,
  `DID6011` varchar(255) DEFAULT NULL,
  `DID6012` int(10) unsigned DEFAULT NULL,
  `DID6013` varchar(255) DEFAULT NULL,
  `DID6014` varchar(255) DEFAULT NULL,
  `DID6015` varchar(255) DEFAULT NULL,
  `DID6016` varchar(255) DEFAULT NULL,
  `DID6017` varchar(255) DEFAULT NULL,
  `DID6050` varchar(255) DEFAULT NULL,
  `DID6051` varchar(255) DEFAULT NULL,
  `DID6052` int(10) unsigned DEFAULT NULL,
  `DID6053` int(10) unsigned DEFAULT NULL,
  `DID6054` varchar(255) DEFAULT NULL,
  `DID1026` bigint(20) DEFAULT NULL,
  `DID1027` bigint(20) DEFAULT NULL,
  `DID1028` bigint(20) DEFAULT NULL,
  `DID1029` int(10) unsigned DEFAULT NULL,
  `DID1030` int(10) unsigned DEFAULT NULL,
  `DID1040` varchar(255) DEFAULT NULL,
  `DID1031` int(10) unsigned DEFAULT NULL,
  `DID1032` int(10) unsigned DEFAULT NULL,
  `DID2009` int(10) unsigned DEFAULT NULL,
  `DID2006` varchar(255) DEFAULT NULL,
  `DID2007` varchar(255) DEFAULT NULL,
  `DID2008` varchar(255) DEFAULT NULL,
  `DID1033` bigint(20) DEFAULT NULL,
  `DID1034` bigint(20) DEFAULT NULL,
  `DID1035` bigint(20) DEFAULT NULL,
  `DID1036` int(10) unsigned DEFAULT NULL,
  `DID1037` int(10) unsigned DEFAULT NULL,
  `DID1041` varchar(255) DEFAULT NULL,
  `DID1038` int(10) unsigned DEFAULT NULL,
  `DID1039` int(10) unsigned DEFAULT NULL,
  `DID2013` int(10) unsigned DEFAULT NULL,
  `DID2010` varchar(255) DEFAULT NULL,
  `DID2011` varchar(255) DEFAULT NULL,
  `DID2012` varchar(255) DEFAULT NULL,
  `DID2030` varchar(255) DEFAULT NULL,
  `DID2031` bigint(20) DEFAULT NULL,
  `DID2032` bigint(20) DEFAULT NULL,
  `DID2033` bigint(20) DEFAULT NULL,
  `DID2034` bigint(20) DEFAULT NULL,
  `DID2035` bigint(20) DEFAULT NULL,
  `DID2036` bigint(20) DEFAULT NULL,
  `DID2037` bigint(20) DEFAULT NULL,
  `DID2060` bigint(20) DEFAULT NULL,
  `DID2061` bigint(20) DEFAULT NULL,
  `DID2062` bigint(20) DEFAULT NULL,
  `DID2046` varchar(255) DEFAULT NULL,
  `DID2047` varchar(255) DEFAULT NULL,
  `DID2048` varchar(255) DEFAULT NULL,
  `DID2049` varchar(255) DEFAULT NULL,
  `DID2050` varchar(255) DEFAULT NULL,
  `DID2051` varchar(255) DEFAULT NULL,
  `DID2052` varchar(255) DEFAULT NULL,
  `DID2053` varchar(255) DEFAULT NULL,
  `DID2054` varchar(255) DEFAULT NULL,
  `DID2055` varchar(255) DEFAULT NULL,
  `DID2056` varchar(255) DEFAULT NULL,
  `DID2057` varchar(255) DEFAULT NULL,
  `DID2058` varchar(255) DEFAULT NULL,
  `DID4100` varchar(255) DEFAULT NULL,
  `DID4101` varchar(255) DEFAULT NULL,
  `DID4102` varchar(255) DEFAULT NULL,
  `DID4103` varchar(255) DEFAULT NULL,
  `DID4104` int(10) unsigned DEFAULT NULL,
  `DID4105` varchar(255) DEFAULT NULL,
  `DID4106` varchar(255) DEFAULT NULL,
  `DID4107` int(10) unsigned DEFAULT NULL,
  `DID4109` varchar(255) DEFAULT NULL,
  `DID4108` int(10) unsigned DEFAULT NULL,
  `DID4111` varchar(255) DEFAULT NULL,
  `phase_icon` blob,
  `user_plane_info` blob,
  `calltype` tinyint(3) unsigned NOT NULL,
  `callstate` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Start_Time` (`Start_Time`,`DID1025`),
  KEY `DID1025` (`DID1025`),
  KEY `Start_Time_2` (`Start_Time`,`DID5503`),
  KEY `DID5503` (`DID5503`),
  KEY `Start_Time_3` (`Start_Time`,`DID5504`),
  KEY `DID5504` (`DID5504`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;")


(defn
  randomCtpTableName
  "Generates a random table name, and creates it the given database"
  [ db-spec ]
  (let [
        year	  (str "20" (format "%02d", (rand-int 18)))
        month     (format "%02d", (inc (rand-int 12)))
        day       (format "%02d",
                          (inc
                            (if (= month "02")
                              (rand-int 28)
                              (rand-int 30))))
        prefix    (str "ctp100" (rand-int 10))
        tableName (str prefix "_" year month day)
        dbName    (get db-spec :dbname)
        sqlStmt   (clojure.string/replace
                    (clojure.string/replace
                      tableTemplate #"::DatabaseName::" dbName)
                    #"::TableName::" tableName)
        ]
    (j/with-db-connection
      [db db-spec]
      (j/execute! db [ sqlStmt ]))
    tableName))


(def tables-chan (async/chan))


(defn start-table-producers
  "Start n threads which will each create 1000 random source tables.
  They are then put on the tables-chan queue to be populated"
  [n db-spec]
  (dotimes [id n]
    (println "Starting table producer thread" (inc id))
    (async/thread
      (dotimes [_ 1000]
        (let [table  (randomCtpTableName db-spec)]
          (async/>!! tables-chan table))))))


(defn start-row-producers
  "Start n threads which will take tables from the table-chan queue
  and populate them with randomly generated field values"
  [n db-spec]
  (dotimes [id n]
    (println "Starting row producer thread" (inc id))
    (async/thread
      (while true
        (let [
              table  (async/<!! tables-chan)
              fields (fields)
              ]
          (j/with-db-connection
            [db db-spec]
            (j/insert! db table fields)))))))




; write random rows to given tables.
(defn generateTestData
  "Creates 2 table producers, each producing 1000 tables
  Creates 1 row producers to produce dummy data for all those tables"
  [ db-spec ]
  (start-table-producers 2 db-spec)
  (start-row-producers   1 db-spec))

