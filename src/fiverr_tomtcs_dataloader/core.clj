(ns fiverr-tomtcs-dataloader.core
  (:gen-class)
  (:use fiverr-tomtcs-dataloader.mysql)
  (:require
    [clojure.string :as str]
    [java-time :as jt]))


(def config (clojure.edn/read-string (slurp "./conf/config.edn")))


(def source-db (assoc (:source-db config) :dbtype "mysql"))
(def target-db (assoc (:target-db config) :dbtype "mysql"))
(def target-table (:target-table config))
(def table-pattern (:source-pattern config))


(defn findNewTables
  "If the config indicates incrementals, then return any tables created
  today and yesterday.
  Otherwise, return all the table names."
  []
  (if (:only-incrementals config)
    (let [tDate       (jt/local-date)
          yDate       (jt/minus tDate (jt/days 1))
          sIncPat     (:source-incremental-pattern config)
          dateFormat  (str/replace sIncPat #"^.*?::([^:]+)::.*$" "$1")
          today       (jt/format dateFormat tDate)
          yesterday   (jt/format dateFormat yDate)
          tTablePat   (str/replace sIncPat #"::\w+::" today)
          yTablePat   (str/replace sIncPat #"::\w+::" yesterday)
          todayTables (getTables source-db tTablePat)
          yestTables  (getTables source-db yTablePat)
          ]
      (concat yestTables todayTables))
    (getTables source-db table-pattern)))


(defn -main
  "Read ctp data from source database and inserts it into the
  all_ctp table in the target db.
  If the configuration is for incrementals, then all the ctp
  tables created yesterday and today are checked.
  Otherwise, all ctp tables are extracted."
  [& args]
  ; Create the target database table if needed
  (createTargetTable target-db target-table)
  (let [
        tables (findNewTables)
        rows   (flatten (for [table tables] (selectStar source-db table)))]
    (doseq [row rows]
      (println (format
                 "Processing row from table: '%s', with ID: '%s'"
                 (:original_table row)
                 (:original_id row)))
      (insert-if-missing!
        target-db
        target-table
        row
        (format
          "original_table = '%s' AND original_id = %s"
          (:original_table row)
          (:original_id row))))))
