(ns fiverr-tomtcs-dataloader.mysql
  (:gen-class)
  (:require [clojure.java.jdbc :as j]))


(defn getTables
  "Gets all tables in the database db-spec that match the given SQL pattern"
  [db-spec pattern]
  (j/with-db-connection
    [db-con db-spec]
    (j/with-db-metadata
      [md db-con]
      (println
        (format
          "Retrieving list of \"%s\" tables from source server." pattern))
      (map
        #(:table_name %)
        (j/metadata-result
          (.getTables
            md nil nil pattern (into-array ["TABLE"])))))))


(defn selectStar
  "Returns a transformed list of all the rows in the database (db-spec)
  for the table (tableName).
  :original_id replaces :id (same value)
  :original_table is added (with the name of the table)"
  [db-spec tableName]
  (j/with-db-connection
    [db db-spec]
    (let [
          rows (j/query db [ (format "SELECT * FROM `%s`" tableName) ])
          prepared-rows (map (fn [row]
                               (-> row
                                   (assoc :original_table tableName)
                                   (clojure.set/rename-keys {:id :original_id})))
                             rows)
          ]
      prepared-rows)))


(defn insert-if-missing!
  "Inserts a new row in the specified table, if is not already there"
  [db-spec table row where-clause]
  (j/with-db-connection
    [db db-spec]
    (let [
          result (j/query
                   db
                   [ (format "SELECT * FROM `%s` WHERE %s" table where-clause)])
          ]
      (if (nil? (first result))
        (j/insert! db table row)))))


(def targetTableTemplate (slurp "./conf/target_table.sql"))

(defn createTargetTable
  "Creates the target table on the target database, if it does not already exist"
  [ db-spec target-table-name ]
  (j/with-db-connection
    [db-con db-spec]
    (j/with-db-metadata
      [md db-con]
      (let [result (j/metadata-result
                     (.getTables
                       md nil nil target-table-name (into-array ["TABLE"])))]
        (if (empty? result)
          (let [create-sql (clojure.string/replace
                             targetTableTemplate
                             #"::TargetTableName::"
                             target-table-name
                             )
                result     (j/execute! db-con [ create-sql ])]
            result)
          "Already existing")))))

